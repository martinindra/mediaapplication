package com.example.soundpollapplication

import android.media.AudioAttributes
import android.media.AudioManager
import android.media.SoundPool
import android.os.Build
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_sound.*

class SoundFragment : Fragment() {

    private lateinit var soundPool: SoundPool

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_sound, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        var playSound : Int = 0

        if (Build.VERSION.SDK_INT >= 21){
            val audioAttrib = AudioAttributes.Builder()
                .setUsage(AudioAttributes.USAGE_GAME)
                .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                .build()
            val builder = SoundPool.Builder()
            builder.setAudioAttributes(audioAttrib).setMaxStreams(1)
            this.soundPool = builder.build()

        }else{
            this.soundPool = SoundPool(1, AudioManager.STREAM_MUSIC, 0)
        }


        playSound = soundPool.load(context, R.raw.sound, 1)

        btnPlaySound.setOnClickListener {
            soundPool.play(
                playSound, 1f, 1f, 0, 1, 1f)
            soundPool.autoPause()
        }
    }
}