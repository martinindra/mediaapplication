package com.example.soundpollapplication

import android.media.AudioAttributes
import android.media.AudioManager
import android.media.SoundPool
import android.media.session.MediaController
import android.net.Uri
import android.os.Build
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_sound.*
import kotlinx.android.synthetic.main.fragment_video.*

class VideoFragment : Fragment() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_video, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val path = "android.resource://"+ context!!.packageName +"/" + R.raw.video
        videoview.setVideoURI(Uri.parse(path))

        btnPlayVideo.setOnClickListener {
            videoview.start()
        }
        btnPauseVideo.setOnClickListener {
            videoview.pause()
        }
        btnStopVideo.setOnClickListener {
            videoview.stopPlayback()
            videoview.setVideoURI(Uri.parse(path))
        }
    }

}

