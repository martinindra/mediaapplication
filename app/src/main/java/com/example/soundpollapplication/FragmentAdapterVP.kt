package com.example.soundpollapplication

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter

class FragmentAdapterVP(fm: FragmentManager): FragmentPagerAdapter(fm) {

    private val pages = listOf(
        SoundFragment(),
        VideoFragment()
    )

    override fun getCount(): Int {
        return pages.size
    }

    override fun getItem(position: Int): Fragment {
        return pages[position]
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return when (position) {
            0 -> "Sound"
            1 -> "Video"
            else -> "Sound"
        }
    }
}